// Author: Olivier Lenoir - <olivier.len02@gmail.com>
// Created: 2022-05-19
// Updated:
// License: MIT, Copyright (c) 2022 Olivier Lenoir
// Language: OpenSCAD 2019.01-RC2
// Project: Egg incubator divider
// Description:


// Special variables
$fa = 5;  // minimum angle
$fs = 0.1;  // minimum size
$fn = undef;  //number of fragments
$t = undef;  // animation step


// Viewport
$vpt = [298, 210, 0];  // translation
$vpr = [45, 0.00, 30];  // rotation angles in degrees
$vpd = 1500;  // camera distance
$vpf = undef;  // camera field of view


// include


// use
use <../submodule/NotchBox/openscad/NotchBox.scad>;


// Main
// Corrugated fiberboard
// Thickness 2.8mm
// Speed 1200mm/min, Power 75%, Pass 7
// Test: projection(cut = false) notch_box(55, 34, 21, 2.8, 6);

projection(cut = false)
notch_box(285 + 2 * 2.8, 193 + 2 * 2.8, 40, 2.8, 6, Div=[6, 4]);


// Modules


// Functions

